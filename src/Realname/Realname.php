<?php

namespace JyRealnameTy\Realname;

use JyRealnameTy\Kernel\Http;

class Realname
{
  use Order;
  
  protected $domainUrl = 'https://api.shumaidata.com';
  protected $config = [];
  protected $error = null;
  
  public function __construct($config = null)
  {
    $this->config = $config;
  }
  
  private function request($url, $params)
  {
    $params         = $this->handleGlobalParam($params);
    $params['sign'] = $this->getSign($params);
    dd($params);
    return $this->post($this->domainUrl . $url, http_build_query($params));
  }
  
  private function requestGet($url, $params = [])
  {
    if (!$this->requestCheck()) {
      return false;
    }
    $params         = $this->handleGlobalParam($params);
    $params['sign'] = $this->getSign($params);
    return Http::httpGet($this->domainUrl . $url, $params);
  }
  
  private function post($url, $data)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //不验证证书
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $headers = [
      'Content-type:application/x-www-form-urlencoded;charset="utf-8"',
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output, true);
  }
  
  /**
   * 计算签名
   *
   * @param $params  请求参数集合，包括公共参数和业务参数
   * @return string md5签名
   */
  private function getSign($params)
  {
    // 排序所有请求参数
    ksort($params);
    $src_value = "";
    $timestamp = $params['timestamp'];
    unset($params['timestamp']);
    foreach ($params as $value) {
      $src_value .= $value;
    }
    //计算md5
    return md5($this->config['secret'] . $src_value . $this->config['secret']);
  }
  
  /**
   * 处理全局参数
   *
   * @param $params
   * @return array
   */
  private function handleGlobalParam($params)
  {
    // 全局参数
    $globalParam = [
      'appid'     => $this->config['appid'],
      'timestamp' => time() . rand(100, 999),
    ];
    return array_merge($globalParam, $params);
  }
  
  private function handleReturn($res)
  {
    if ($res['code'] == 200) {
      $this->error = null;
      return $res;
    }
    $this->setError($res);
    return false;
  }
  
  public function getError()
  {
    return $this->error;
  }
  
  private function setError($error)
  {
    $this->error = $error;
    return false;
  }
  
  private function requestCheck()
  {
    if (!$this->config['key']) {
      $this->setError('请设置config信息，key');
      return false;
    } elseif (!$this->config['secret']) {
      $this->setError('请设置config信息，secret');
      return false;
    } elseif (!$this->session) {
      $this->setError('请先设置session');
      return false;
    }
    return true;
  }
}

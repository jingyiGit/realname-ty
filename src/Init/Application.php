<?php

namespace JyRealnameTy\Init;

use JyRealnameTy\Kernel\Response;
use JyRealnameTy\Realname\Realname;
use JyRealnameTy\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Realname
{
  use Response;
  use BaseConfig;
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
